import React from "react";
import { HashRouter, Route, Switch } from "react-router-dom";
import Home from "./pages/Home";
import Dashboard from "./pages/Dashboard";
import CourseDetails from "./pages/CourseDetail/index";
import Course from "./pages/Course/index";

export default class Routes extends React.Component<any> {
  render(){
  	return (
    	<HashRouter>
				<Switch>
					<Route path={"/"} exact component={Home} />
					<Route path={"/dashboard"} exact component={Dashboard} />
					<Route path={"/course-details"} exact component={CourseDetails} />
					<Route path={"/course"} exact component={Course} />
				</Switch>
    	</HashRouter>
    );
  }
}