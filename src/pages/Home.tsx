import React, { Component } from 'react';
import styled, { injectGlobal } from 'styled-components';
import {Clearfix, Col, Grid, Panel, Row} from 'react-bootstrap';
import brandLogo from '../assets/images/brand_logo.svg';
import TreasureLogo from '../assets/images/treasurehunt_logo.svg';
import ABGLogo from '../assets/images/ABG_logo.png';
import { Button, Input } from '../components/FormElements';

const BgImgContainer = styled.div `
    position: absolute;
    height: 100%;
    width: 100%;
    top: 0;
    left: 0;
    :after{
        content: '';
        position: absolute;
        height: 100%;
        width: 100%;
        top: 0;
        left: 0;
        opacity: 0.76;
        background-image: linear-gradient(119deg, #2a1e1a, #3d3835);
    }
`;

const BgHome = styled.div `
    height: 100%;
    width: 100%;
    background-size: cover;
    background-position: center;
    background-repeat: no-repeat;
    background-image: url(${require('../assets/images/bg1.jpg')});
`;

class Home extends React.Component<any> {
    constructor(props:any){
        super(props);
    }

    LoggedIn = () => {
        this.props.history.push('/dashboard')
    }

    componentDidMount = () => {
        document.body.style.padding = '0';
    }

    render() {
        return (
            <section>
                <BgImgContainer>
                    <BgHome></BgHome>
                </BgImgContainer>
                <Grid className="AtomsContainer d-flex align-items-center">
                    <Row className="d-flex justify-content-between">
                        <Col md={6} className="d-flex flex-column justify-content-center">
                            <div>
                                <img src={brandLogo} />
                            </div>
                            <div className="my-5">
                                <p className="font30 text-white font-light">Advanced Data Analytics Platform with Powerful Experiential Learning</p>
                            </div>
                            <div>
                                <p className="font12 text-white mb-2">Powered by</p>
                                <img src={TreasureLogo}/>
                            </div>
                        </Col>
                        <Col md={6} className="d-flex justify-content-end">
                            <div className="width350px white-bg px-5 py-4 text-center border-radius-4">
                                <p className="text-center font26 font-bold mb-5">Log In</p>
                                <div className="form-group">
                                    <input type="email" placeholder="Email" className="form-control" />
                                </div>
                                <div className="form-group">
                                    <Input inputType="password" className="form-control mb-4" placeHolder="Password" />
                                </div>
                                <div className="form-group m-0">
                                    <Button className="btn-lg btn-primary text-uppercase my-3" onClick={this.LoggedIn}>Log in</Button>
                                </div>
                                <div className="form-group mb-5">
                                    <a className="btn btn-link font14 font-bold secondary-text" href="/">Forgot Password ?</a>
                                </div>
                                <div className="form-group m-0">
                                    <img className="width100px" src={ABGLogo}/>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </Grid>
            </section>
        );
    }
}

export default Home;