import React, { Component } from 'react';
import '../../assets/sass/module/Dashboard.scss';
import createProjImg from '../../assets/images/create_project.svg';
import TreasureLogo from '../../assets/images/treasurehunt_logo.svg';
import ProjImg1 from '../../assets/images/project1.svg';
import { Button, Input } from '../../components/FormElements';
import styled from 'styled-components';
import { Clearfix, Col, Grid, Panel, Row } from 'react-bootstrap';
import HeaderMenu from '../../components/HeaderMenu';
import Scrollbars from 'react-custom-scrollbars';
import { Link } from 'react-router-dom';

const ScrollbarWithMenu = styled.section `
    height: calc(100vh - 60px);
`;

const BgImgContainer = styled.div `
    position: absolute;
    height: 100%;
    width: 100%;
    top: 0;
    left: 0;
    :after{
        content: '';
        position: absolute;
        height: 100%;
        width: 100%;
        top: 0;
        left: 0;
        opacity: 0.76;
        background-image: linear-gradient(119deg, #2a1e1a, #3d3835);
    }
`;

const BgDashboard = styled.div `
    height: 100%;
    width: 100%;
    background-size: cover;
    background-position: center;
    background-repeat: no-repeat;
    background-image: url(${require('../../assets/images/bg2.jpg')});
`;

const ExplorerTag = styled.div `
    padding: 11px 16px;
    height: 64px;
    border-radius: 32px;
`;

const ExplorerContainer = styled.div `
    display: flex;
    align-items: center;
    width: 100%;
`;

const ExplorerContent = styled.div `
    display: flex;
    flex-direction: column;
`;

const DashboardCards = styled.div ` 
    width: 555px;
    padding: 0 16px;
    float: left;
`;

const CompletedRange = styled.div `
    height: 7px;
    background-color: rgba(195, 195, 195, 0.2);
    margin: 5px 16px 15px 0;
    width: 100%;
`;

export default class Dashboard extends Component<any,any> {
    
    constructor(props:any){
        super(props);
        this.state = {
            resume: false,
        }
    }

    handleResume = () => {
        this.setState({
            resume: !this.state.resume
        })
    }

    render() {
        
        const props: any = [{
            'name': 'John',
            'completed': '20',
            'lastCompleted': 'Basic description',
            'totalPoints': '10',
            'levelNumber': '01',
            'date': '27 Dec 2018',
            'courseName': 'Fundamentals of Data Analytics'
        }];

        let completedRange = this.state.resume
            ?   <div className="mb-4">
                    <div className="d-flex mb-3">
                        <CompletedRange className="col p-0 border-radius-4">
                            <div className="primary-bg border-radius-4 height100" style={{width: props[0].completed+'%'}}></div>
                        </CompletedRange>
                        <div className="width90px font12">{props[0].completed}% Completed</div>
                    </div>
                    <div className="secondary-text mb-1 font16 d-flex w-100 align-items-center">
                        <span className="CompletedLabel">Last Completed :</span> <i className="icon icon-play-outline-sm mr-1"></i>
                        <span className="d-inline-block text-truncate width130px">{props[0].lastCompleted}</span>
                    </div>
                </div>
            : <p className="secondary-text mb-5">Learn and apply statistical concepts to interpret data and solve problems</p>

        return (
            <ScrollbarWithMenu>
                <HeaderMenu />
                <BgImgContainer>
                    <BgDashboard></BgDashboard>
                </BgImgContainer>
                <Scrollbars className="custom-scrollbars">
                    <Grid>
                        <Row className="DashboardContainer">
                            <Col md={12} className="mt-5 mb-4">
                                <h4 className="white-text">Hello, Mr. {props[0].name}</h4>
                            </Col>
                            <Col md={12}>
                                <div className="d-flex mb-5">
                                    <div className="width100px d-flex flex-column align-items-center mr-5">
                                        <div className="white-text font-bold mb-3 font16">Total Points</div>
                                        <div className="TotalPoints white-bg text-center d-inline-block font22 font-bold border-radius-100cent">
                                            <div className="TotalPointsNumber">{props[0].totalPoints}</div>
                                        </div>
                                    </div>
                                    <div className="d-flex flex-column">
                                        <div className="white-text font-bold mb-3 font16">Atoms Explorer</div>
                                        <ExplorerTag className="white-bg width200px d-inline-block">
                                            <ExplorerContainer>
                                                <div className="mr-2">
                                                    <i className="icon icon-atoms-explorer"></i>
                                                </div>
                                                <ExplorerContent>
                                                    <div><p className="ExplorerTitle font15 font-bold m-0">Level : {props[0].levelNumber}</p></div>
                                                    <div><p className="ExplorerSub sub-secondary-text font11 m-0">Date : {props[0].date}</p></div>
                                                </ExplorerContent>
                                            </ExplorerContainer>
                                        </ExplorerTag>
                                    </div>
                                </div>
                                <Row>
                                    <DashboardCards className="mb-4">
                                        <div className="card h-100 border-radius-10">
                                            <div className="card-body d-flex">
                                                <div className="col p-4">
                                                    <div>
                                                        <h3 className="secondary-text mb-4 mt-2">Fundamentals of Data Analytics</h3>
                                                        { completedRange }
                                                    </div>
                                                    <div>
                                                        {this.state.resume
                                                            ? <Link to="/course-details" className="btn btn-lg btn-outline">Resume</Link>
                                                            : <Button className="btn-lg btn-outline" onClick={this.handleResume}>Get Started</Button> }
                                                    </div>
                                                </div>
                                                <div className="width150px p-3">
                                                    <img src={ProjImg1}/>
                                                </div>
                                            </div>
                                        </div>
                                    </DashboardCards>
                                    <DashboardCards className="mb-4">
                                        <div className="card h-100 border-radius-10">
                                            <div className="card-body d-flex">
                                                <div className="col p-4 d-flex flex-column justify-content-between">
                                                    <div>
                                                        <h3 className="secondary-text mb-4 mt-2">Create your own project</h3>
                                                        <p className="secondary-text mb-5">Bring your own data and get started with solving practical problems.</p>
                                                    </div>
                                                    <div>
                                                        <Button className="btn-lg btn-outline disabled">Create</Button>
                                                    </div>
                                                </div>
                                                <div className="width150px p-3">
                                                    <img src={createProjImg}/> 
                                                </div>
                                            </div>
                                        </div>
                                    </DashboardCards>
                                </Row>
                            </Col>
                            <Col md={12} className="text-center my-5">
                                <span className="font15 white-text mr-4">Powered by</span>
                                <img src={TreasureLogo}/>
                            </Col>
                        </Row>
                    </Grid>
                </Scrollbars>
            </ScrollbarWithMenu>
        );
    }
}