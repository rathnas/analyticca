import React, { Component } from 'react';
import Scrollbars from 'react-custom-scrollbars';
import '../../assets/sass/module/Course.scss';
import File from './lessons/file';
import Default from './lessons/default_lesson';
import Information from './lessons/information';
import Quiz from './lessons/quiz';
import Simulation from './lessons/simulation';
import Video from './lessons/video';
import Webinar from './lessons/webinar';

export default class LessonWidget extends Component<any, any> {
    constructor(props:any){
        super(props);
    }

    render(){
        let lessonComponent:any;
        switch(this.props.selectedLesson[0].type) {
            case 'information':
                lessonComponent = <Information />
                break;
            case 'file':
                lessonComponent = <File />
                break;
            case 'simulation':
                lessonComponent = <Simulation/>
                break;
            case 'quiz':
                lessonComponent = <Quiz/>
                break;
            case 'video':
                lessonComponent = <Video/>
                break;                
            case 'webinar':
                lessonComponent = <Webinar/>
                break;
            default:
                lessonComponent = <Default />
        }
        return(
            <div className="LessonWidget white-bg">
            {this.props.selectedLesson[0].title == null ? '' :
                <div className="LessonTitle d-flex justify-content-between font20 font-bold">
                    <div>{this.props.selectedLesson[0].title}</div>
                    {this.props.selectedLesson[0].type == 'file' ? <div className="PDFHeaderOptions"><i className="icon icon-download mr-4"></i><i className="icon icon-print mr-4"></i></div> : ''}
                </div>
                }
                <div className={"LessonContentWidget" + (this.props.selectedLesson[0].type == null ? ' default' : '')}>
                    <Scrollbars className="custom-scrollbars">
                        {lessonComponent}
                    </Scrollbars>
                </div>
            </div>
        );
    }
}