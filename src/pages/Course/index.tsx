import React, { Component } from 'react';
import '../../assets/sass/module/Course.scss';
import HeaderMenu from '../../components/HeaderMenu';
import {Clearfix, Col, Grid, Panel, Row} from 'react-bootstrap';
import { CourseListWidget } from './courseListWidget';
import styled, { injectGlobal } from 'styled-components';
import * as Loader from 'react-loader';
import LessonWidget from './LessonWidget';

const CourseWidget = styled.div `
    display: flex;
    align-items: center;
    overflow: hidden;
`;

injectGlobal`
  body {
    background-color: #fbfbfb
  }
`;

const ListWidget = styled.div `
    position: relative;
    width: 314px;
    height: calc(100vh - 60px);
    padding: 30px 0 0 30px;
    margin-right: 6px;
`;

const CourseDetailWidget = styled.div `
    width: calc(100% - 320px);
    height: calc(100vh - 60px);
    overflow: hidden;
    padding: 30px 30px 30px 10px;
`

export default class Course extends Component<any, any> {
    constructor(props:any){
        super(props);
        this.state = {
            course: [
                {
                    title: null,
                    type: null
                }
            ]
        }
    }

    handleCourses = (courseTitle: any, courseType: any) => {
        let courses = this.state.course.map((courseDetails: any) => {
            courseDetails.title = courseTitle;
            courseDetails.type = courseType;
            return courseDetails;
        });
        this.setState({
            course: courses
        })
    }

    render(){
        return(
            <section>
                <HeaderMenu/> 
                <CourseWidget>
                    <ListWidget>
                        <CourseListWidget courseType={this.handleCourses}/>
                    </ListWidget>
                    <CourseDetailWidget>
                        <LessonWidget selectedLesson={this.state.course}/>
                    </CourseDetailWidget>
                </CourseWidget>
            </section>
        );
    }
}