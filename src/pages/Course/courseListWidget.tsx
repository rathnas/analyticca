import React from 'react';
import Scrollbars from 'react-custom-scrollbars';
import { UncontrolledCollapse} from 'reactstrap';
import styled, { injectGlobal } from 'styled-components';
import { Link } from 'react-router-dom';

const CourseListContainer = styled.div `
    position: relative;
    height: 100%;
    user-select: none;
    margin: 0;
`;

const SubCourseListWidget = styled.div `
    position: relative;
    top: -5px;
    height: calc(100% - 60px);
    overflow: hidden;
`;

const CourseListItem = styled.div `
    display: inline-block;
    border: 0;
    background: #ffffff;
    border-radius: 4px 4px 0 0;
    width: calc(100% - 15px);
    color: #4b433e;
    margin: 0 0 0 5px;
    padding: 20px 30px 20px 20px;
    box-shadow: 0 -1px 4px rgba(0,0,0,.14);
    z-index: 1;
`;

export class CourseListWidget extends React.Component<any, any> {
    constructor(props:any){
        super(props);
        this.state = {
            course_title: 'Fundamentals of Data Analytics and',
            courseList:[
                {
                    title: 'Week 1-2',
                    courseId: '1',
                    active: false,
                    lock: false,
                    courses: [
                        {
                            title: 'Overview',
                            lessonId: '11',
                            sub_title: '',
                            type: 'information',
                            completed: true,
                            active: false
                        },
                        {
                            title: 'Fundamentals of Data Analysis',
                            lessonId: '12',
                            sub_title: '',
                            type: 'video',
                            completed: true,
                            active: false
                        },
                        {
                            title: 'Practice Simulation 1',
                            lessonId: '13',
                            sub_title: 'Guided',
                            type: 'simulation',
                            completed: false,
                            active: false
                        },
                        {
                            title: 'Practice Simulation 2',
                            lessonId: '14',
                            sub_title: 'Self',
                            type: 'simulation',
                            completed: false,
                            active: false
                        },
                        {
                            title: 'Basic Numerative Descriptive Measures',
                            lessonId: '15',
                            sub_title: '',
                            type: 'file',
                            completed: false,
                            active: false
                        },
                        {
                            title: 'Webinar 1',
                            lessonId: '16',
                            sub_title: '02 Jan 2019, 3 PM - 5 PM',
                            type: 'webinar',
                            completed: false,
                            active: false
                        },
                        {
                            title: 'Quiz',
                            lessonId: '17',
                            sub_title: '02 Jan 2019, 3 PM - 5 PM',
                            type: 'quiz',
                            completed: false,
                            active: false
                        }
                    ]
                },
                {
                    title: 'Week 3-4',
                    courseId: '2',
                    active: false,
                    lock: true,
                    courses: [
                        {
                            title: 'overview',
                            lessonId: '21',
                            sub_title: '',
                            type: 'information',
                            completed: false,
                            active: false
                        },
                        {
                            title: 'Fundamentals of Data Analysis',
                            lessonId: '22',
                            sub_title: '',
                            type: 'video',
                            completed: false,
                            active: false
                        },
                        {
                            title: 'Practice Simulation 1',
                            lessonId: '23',
                            sub_title: 'Guided',
                            type: 'simulation',
                            completed: false,
                            active: false
                        },
                        {
                            title: 'Practice Simulation 2',
                            lessonId: '24',
                            sub_title: 'Self',
                            type: 'simulation',
                            completed: false,
                            active: false
                        },
                        {
                            title: 'Basic Numerative Descriptive Measures',
                            lessonId: '25',
                            sub_title: '',
                            type: 'file',
                            completed: false,
                            active: false
                        },
                        {
                            title: 'Webinar 1',
                            lessonId: '26',
                            sub_title: '02 Jan 2019, 3 PM - 5 PM',
                            type: 'webinar',
                            completed: false,
                            active: false
                        },
                        {
                            title: 'Quiz',
                            lessonId: '27',
                            sub_title: '02 Jan 2019, 3 PM - 5 PM',
                            type: 'quiz',
                            completed: false,
                            active: false
                        }
                    ]
                },
                {
                    title: 'Week 5-6',
                    courseId: '3',
                    active: false,
                    lock: true,
                    courses: [
                        {
                            title: 'overview',
                            lessonId: '31',
                            sub_title: '',
                            type: 'information',
                            completed: false,
                            active: false
                        },
                        {
                            title: 'Fundamentals of Data Analysis',
                            lessonId: '32',
                            sub_title: '',
                            type: 'video',
                            completed: false,
                            active: false
                        },
                        {
                            title: 'Practice Simulation 1',
                            lessonId: '33',
                            sub_title: 'Guided',
                            type: 'simulation',
                            completed: false,
                            active: false
                        },
                        {
                            title: 'Practice Simulation 2',
                            lessonId: '34',
                            sub_title: 'Self',
                            type: 'simulation',
                            completed: false,
                            active: false
                        },
                        {
                            title: 'Basic Numerative Descriptive Measures',
                            lessonId: '35',
                            sub_title: '',
                            type: 'file',
                            completed: false,
                            active: false
                        },
                        {
                            title: 'Webinar 1',
                            lessonId: '36',
                            sub_title: '02 Jan 2019, 3 PM - 5 PM',
                            type: 'webinar',
                            completed: false,
                            active: false
                        },
                        {
                            title: 'Quiz',
                            lessonId: '37',
                            sub_title: '02 Jan 2019, 3 PM - 5 PM',
                            type: 'quiz',
                            completed: false,
                            active: false
                        }
                    ]
                }
            ]
        }
    }

    handleCourseTitleClick = (courseId: any) => {
        let courses = this.state.courseList.map((courseList: any) => {
            if(courseList.courseId === courseId && courseList.lock == false){
                courseList.active = !courseList.active
            }
            return courseList;
        });
        this.setState({
            courseList: courses
        })
    }
    

    handleLessonTitleClick = (lessonId: any, courseId: any) => {
        let courses = this.state.courseList.map((courseList: any) => {
            if(courseList.courseId === courseId){
                courseList.courses.map((lessonList: any) => {
                    if(lessonList.lessonId === lessonId){
                        lessonList.active = true;
                        this.props.courseType(lessonList.title, lessonList.type);
                    } else {
                        lessonList.active = false
                    }
                    return lessonList;
                })
            } else {
                courseList.courses.map((lessonList: any) => {
                    lessonList.active = false
                })
            }
            return courseList;
        });
        this.setState({
            courseList: courses
        })
    }

    render(){

        let courseList = this.state.courseList.map((courseContent:any) => {
            let courseLock = courseContent.lock ? 'lock' : '';
            let courseActive = courseContent.active ? 'active' : '';
            return(
                <li className={"list-group-item subCourseListItem"} key={courseContent.courseId}>
                    <p className={"subCourseLink mb-0" + " " + courseActive + " " + courseLock} id={"courseList" + courseContent.courseId} onClick={this.handleCourseTitleClick.bind(this, courseContent.courseId)}>
                        {courseContent.title}
                    </p>

                    {courseContent.lock ? '' :
                        <UncontrolledCollapse toggler={"#courseList" + courseContent.courseId} className="subCourseContent" isOpen={courseContent.active}>
                            <ul className="list-group subCourseContentItem" >
                                {courseContent.courses.map((courseItem:any) => {
                                    let completed = courseItem.completed ? 'completed' : '';
                                    let active = courseItem.active ? 'active' : '';
                                    return(
                                        <li
                                            className={"list-group-item subCourseContentLink " + completed + " " + active}
                                            key={courseItem.lessonId}
                                            onClick={this.handleLessonTitleClick.bind(this, courseItem.lessonId, courseContent.courseId)}
                                            >
                                            <div className="d-flex align-items-center">
                                                <div>
                                                    <i className={"icon mr-3 icon-" + courseItem.type + ' ' + active}></i>
                                                </div>
                                                <div className="d-flex flex-column">
                                                    <div className="LessonTitle">
                                                        {courseItem.title}
                                                    </div>
                                                    <div className="LessonSub">
                                                        {courseItem.sub_title}
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    )
                                })}
                            </ul>
                        </UncontrolledCollapse> }
                </li>
            )
        })

        return(
            <CourseListContainer>
                <CourseListItem className="list-group-item text-truncate height60px font13 font-bold">
                    <Link to="/course-details"><i className="icon icon-left-arrow-md mr-2"></i></Link> {this.state.course_title}
                </CourseListItem>
                
                <SubCourseListWidget>
                    <Scrollbars className="custom-scrollbars">
                        <ul className="list-group subCourseListContainer">
                            {courseList}
                        </ul>
                    </Scrollbars>
                </SubCourseListWidget>
            </CourseListContainer>
        );
    }
}