import React, { Component } from 'react';
import { Button } from '../../../components/FormElements';

export default class Simulation extends Component<any, any> {
    constructor(props:any){
        super(props);
    }

    render(){
        return(
            <div className="LessonContentContainer">
                <p className="font14 mb-5">Integer ac mollis risus. Donec tincidunt enim leo, sit amet congue libero molestie at. Maecenas dui elit, pretium quis ante volutpat, gravida scelerisque dolor. Cras laoreet fringilla sodales. Maecenas quis tincidunt orci. In mollis dolor vel ligula malesuada facilisis. Mauris massa massa, porta ac diam at, blandit luctus tellus. Donec dolor ligula, volutpat id felis at, sagittis ultrices elit. Morbi sollicitudin enim fermentum posuere rhoncus. Curabitur neque dui, malesuada tincidunt nisi id, euismod hendrerit risus. Ut a pulvinar nulla. Phasellus egestas congue mauris, eget pretium ipsum fringilla sit amet. Vivamus vitae purus blandit, luctus ante sit amet, malesuada metus</p>
                <Button className="btn btn-lg btn-outline">Start</Button>
            </div>
        );
    }
}