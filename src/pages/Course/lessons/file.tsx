import React, { Component } from 'react';

export default class File extends Component<any, any> {
    constructor(props:any){
        super(props);
        this.state = {
            pdfUrl: require('../../../assets/sample.pdf?scrollbar=0')
        }
    }

    render(){
        return(
            <div className="LessonContentContainer p-0 h-100">
                <embed src={this.state.pdfUrl} type="application/pdf" width="100%" height="99%" />
            </div>
        );
    }
}