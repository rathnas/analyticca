import React, { Component } from 'react';
import { Button } from '../../../components/FormElements';
import ModalComponent from '../../../components/ModalComponent';
import {TimerProgressbar} from '../../../components/TimerProgressBar';
import Scrollbars from 'react-custom-scrollbars';
import {Link} from 'react-router-dom';
import 'react-circular-progressbar/dist/styles.css';

export default class Quiz extends Component<any, any> {
    constructor(props:any){
        super(props);
        this.state={
            isOpenModal: false,
            quizQuestions: [
                {
                    Question: 'What is the standard deviation for AAPL daily log return?',
                    Answer: ['0.2367', '0.0323', '0.0376', '0.3206'],
                    marks: '05'
                },
                {
                    Question: 'Which stock is more volatile?',
                    Answer: ['MSFT', 'AAPL'],
                    marks: '05'
                },
                {
                    Question: 'Which stock has higher IQR?',
                    Answer: ['AAPL', 'MSFT'],
                    marks: '05'
                },
                {
                    Question: 'What is the standard deviation for AAPL daily log return? ',
                    Answer: ['0.2367', '0.0323', '0.0376', '0.3206'],
                    marks: '05'
                }
            ]
        }
    }

    toggleModal = () => {
        this.setState({
            isOpenModal: !this.state.isOpenModal
        });
    }
    
    render(){
        return(
            <div>
                <div className="LessonContentContainer">
                    <div className="d-flex mb-4">
                        <div className="width200px">
                            <h3 className="mb-3 darkGray-text">4</h3>
                            <span className="font-bold lightGray-text">No of Questions</span>
                        </div>
                        <div className="width200px">
                            <h3 className="mb-3 darkGray-text">20</h3>
                            <span className="font-bold lightGray-text">Total Marks Alloted</span>
                        </div>
                        <div className="width200px">
                            <h3 className="mb-3 darkGray-text">8 min 30 sec</h3>
                            <span className="font-bold lightGray-text">Time</span>
                        </div>
                    </div>
                    <div className="mb-4">
                        <p className="font-bold mb-3">Instructions</p>
                        <ul className="numberedList">
                            <li className="numberedItem mb-2">Quiz can only be attempted maximum two times.</li>
                            <li className="numberedItem mb-2">Score of the quiz will be best of all attempts.</li>
                            <li className="numberedItem mb-2">There can be questions from different difficulty levels.</li>
                            <li className="numberedItem mb-2">You won't be able to attempt a question after you start the quiz.</li>
                            <li className="numberedItem mb-2">There won't be any provision to stop or pause the quiz once you start the quiz.</li>
                            <li className="numberedItem mb-2">Quiz will auto submit if you run out of time.</li>
                            <li className="numberedItem mb-2">Timer can not be stopped or paused once you start the quiz.</li>
                            <li className="numberedItem mb-2">Wish you our very best!</li>
                        </ul>
                    </div>
                    <div>
                        <Button className="btn btn-lg btn-outline" onClick={this.toggleModal}>Start Quiz</Button>
                    </div>
                </div>


                {/* ----------------------------------- Quiz Modal Box ----------------------------------- */}

                <ModalComponent
                    className="modal-lg QuizContainer"
                    isOpenModal={this.state.isOpenModal}
                    toggleModal={this.toggleModal}
                    ModalTitle="Quiz"
                    >
                    <div className="QuizContent d-flex justify-content-between align-items-start">
                        <div className="InstructionWidget">                   
                            <Scrollbars className="custom-scrollbars">
                                <div className="MarksQuestionWidget">
                                    <div className="MarksQuestions d-flex justify-content-between">
                                        <div className="Marks">
                                            <span className="d-block font30 font-bold darkGray-text">20</span>
                                            <span className="d-block font16 font-bold lightGray-text">Total Marks</span>
                                        </div>
                                        <div className="Questions">
                                            <span className="d-block font30 font-bold darkGray-text">04</span>
                                            <span className="d-block font16 font-bold lightGray-text">Questions</span>
                                        </div>
                                    </div>
                                    <div className="ProgressTimerWidget">
                                        <TimerProgressbar timer="0:60"/>
                                    </div>
                                    <div className="InstructionContainer">
                                        <p className="InstructionTitle font16 darkGray-text font-bold mb-2">Instructions</p>
                                        <div className="InstructionContent">
                                            <div className="InstructionItem font12 lightGray-text font-light mb-3">
                                                Quiz can only be attempted maximum two times. 
                                            </div>
                                            <div className="InstructionItem font12 lightGray-text font-light mb-3">
                                                Score of the quiz will be best of all attempts.
                                            </div>
                                            <div className="InstructionItem font12 lightGray-text font-light mb-3">
                                                There can be questions from different difficulty levels.
                                            </div>
                                            <div className="InstructionItem font12 lightGray-text font-light mb-3">
                                                You won't be able to attempt a question after you start the quiz.
                                            </div>
                                            <div className="InstructionItem font12 lightGray-text font-light mb-3">
                                                There won't be any provision to stop or pause the quiz once you start the quiz.
                                            </div>
                                            <div className="InstructionItem font12 lightGray-text font-light mb-3">
                                                Quiz will auto submit if you run out of time.
                                            </div>
                                            <div className="InstructionItem font12 lightGray-text font-light mb-3">
                                                Timer can not be stopped or paused once you start the quiz.
                                            </div>
                                            <div className="InstructionItem font12 lightGray-text font-light">
                                                Wish you our very best!
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </Scrollbars>
                        </div>
                        <div className="MainWidget">
                            <Scrollbars className="custom-scrollbars">
                                <div className="QuizAnswerWidget">
                                    <div className="QuizAnswerContainer">
                                        {this.state.quizQuestions.map((quiz:any, index:any) => {
                                            return(
                                                <div className="quizTemplateWidget" key={index}>
                                                    <div className="quizQuestion d-flex justify-content-between">
                                                        <div className="Question font16 font-bold">{quiz.Question}</div>
                                                        <div className="Marks font14 font-bold">({quiz.marks} Marks)</div>
                                                    </div>
                                                    <div className="AnswerWidget">
                                                        {quiz.Answer.map((answer:any, i:any)=>{
                                                            return(
                                                            <div className="AnswerItem radio">
                                                                <input id={'answer' + index + i} type="radio" name={"optradio"+index}/>
                                                                <label htmlFor={'answer' + index + i} className="AnswerLabel font14">{answer}</label>
                                                            </div>)
                                                        })}
                                                    </div>
                                                </div>
                                            )
                                        })}
                                        <div className='p-4'>
                                            <Link to="/" className="btn btn-lg btn-outline">Finish Test</Link>
                                        </div>
                                    </div>
                                </div>
                            </Scrollbars>
                        </div>
                    </div>
                </ModalComponent>
            </div>
        );
    }
}