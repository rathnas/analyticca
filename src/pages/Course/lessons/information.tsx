import React, { Component } from 'react';

export default class Information extends Component<any, any> {
    constructor(props:any){
        super(props);
    }

    render(){
        return(
            <div className="InformationContent LessonContentContainer">
                <p className="font16 font-bold">Introduction to Basic Numeric Descriptive Measures</p>
                <div className="font14 mb-5">In this module, you will learn basic descriptive measures. You will learn both univariate measures of central tendency (like mean, median, mode, 
standard deviation) as well as bi-variate statistic like covariance and correlation.</div>
                <p className="font14 mb-2"><span className="font-bold">Objective :</span>Learn basic Numeric descriptive measures</p>
                <p className="font14 mb-5"><span className="font-bold">Module pre-requisites :</span>Basic knowledge of algebra</p>
                <p className="font14 font-bold">PLEASE CLICK ON DETAILS TO GET THE CONTEXT OF A PARTICULAR BUSINESS CASE AND ASSOCIATED DATA</p>
                <p className="font14 font-bold">PLEASE CLICK ON MILESTONE - 1 (Bottom of the data) TO START SOLVING A BUSINESS CASE</p>
            </div>
        );
    }
}