import React, { Component } from 'react';

export default class Default extends Component<any, any> {
    constructor(props:any){
        super(props);
    }

    render(){
        return(
            <div className="LessonContentContainer h-100 d-flex align-items-center justify-content-center">
                <div className="d-flex flex-column align-items-center">
                    <div className="mb-5"><img src={require('../../../assets/images/courseDefault.svg')}/></div>
                    <div className="font20">Select the first week to start the course</div>
                </div>
            </div>
        );
    }
}