import React, { Component } from 'react';
import { Button } from '../../../components/FormElements';

export default class Webinar extends Component<any, any> {
    constructor(props:any){
        super(props);
        this.state = {
            recordedWebinar: false
        }
    }

    handleRecordedWebminar = () => {
        this.setState({
            recordedWebinar: !this.state.recordedWebinar
        })
    }

    render(){
        return(
            <div className="LessonContentContainer">
                <div className="d-flex mb-4">
                    <div className="width200px">
                        <h3 className="mb-3 darkGray-text">02 Jan 2019</h3>
                        <span className="font-bold lightGray-text">Date</span>
                    </div>
                    <div className="width200px">
                        <h3 className="mb-3 darkGray-text">3 PM - 5 PM</h3>
                        <span className="font-bold lightGray-text">Time</span>
                    </div>
                </div>
                <div className="d-flex mb-4">
                    <div>
                        <p className="font-bold darkGray-text mr-4">Topic: </p>
                    </div>
                    <div>
                        <p className="mb-2 darkGray-text">Quick recap of concepts and simulations covered during in-person session at Gyanoday</p>
                        <p className="mb-2 darkGray-text">Step-by-step walkthrough of the first simulation</p>
                        <p className="mb-2 darkGray-text">Discussion on the content of previous week</p>
                        <p className="mb-2 darkGray-text">Overview of the content to be covered in the next week</p>
                    </div>
                </div>
                {this.state.recordedWebinar ? 
                    <div className="d-flex">
                        <Button className="btn btn-lg btn-outline" onClick={this.handleRecordedWebminar}>View Recorded Webinar</Button>
                    </div>
                    :
                    <div className="d-flex align-items-center">
                        <div className="mr-4">
                            <Button className="btn btn-lg btn-outline" onClick={this.handleRecordedWebminar}>Join</Button>
                        </div>
                        <div>
                            <p className="m-0">Or follow the link below to join the webinar -</p>
                            <a className="btn-link" href="">https://zoom.us/meeting/register/0977e390f2a696b5dc2496ba44984b7z</a>
                        </div>
                    </div>
                }
            </div>
        );
    }
}