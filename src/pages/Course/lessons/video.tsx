import React, { Component } from 'react';
import VideoPlayer from '../../../components/VideoPlayer';

export default class Video extends Component<any, any> {
    constructor(props:any){
        super(props);
    }

    render(){

        const videoJsOptions: any = {
            autoplay: false,
            controls: true,
            sources: [{
              src: 'http://vjs.zencdn.net/v/oceans.mp4',
              type: 'video/mp4'
            }]
        };
        return(
            <div className="LessonContentContainer">
                <VideoPlayer {...videoJsOptions} />
            </div>
        );
    }
}