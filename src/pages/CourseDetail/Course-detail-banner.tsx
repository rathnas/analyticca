import React, { Component } from 'react';
import ModalComponent from '../../components/ModalComponent';
import {Clearfix, Col, Grid, Panel, Row} from 'react-bootstrap';
import styled, { injectGlobal } from 'styled-components';
import {Link} from 'react-router-dom';
import VideoPlayer from '../../components/VideoPlayer';
import '../../assets/sass/module/Dashboard.scss';


const CourseBannerWrapper = styled.div `
    background: url(${require('../../assets/images/bg1.jpg')}) no-repeat;
    background-size: cover;
    background-position: center;
    position: relative;
    padding: 60px 0;
    color: #ffffff;
    &:before{
        content: "";
        background-image: linear-gradient(to bottom, #2a1e1a, #3d3835);
        opacity: .9;
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        z-index: 0;
    }
`;

const CourseBannerDetail = styled.div `
    position: relative;
`;

const ReturnBack = styled.i `
    position: absolute;
    top: 0;
    left: -80px;
`;

const CourseBannerDetailVideo = styled.div `
    width: 350px;
    height: 200px;
    overflow: hidden;
    position: relative;
    cursor: pointer;
    box-shadow: 0 6px 10px 0 rgba(0, 0, 0, 0.16);
    @include border-radius(5px);

    &:before{
        content: '';
        position: absolute;
        height: 62px;
        width: 62px;
        top: calc(50% - 31px);
        left: calc(50% - 31px);
        background: url(${require('../../assets/images/spritesheet.svg')}) no-repeat -1px -497px;
    }

    img{
        width: 100%;
    }

    video {
        width: 100%;
    }
`;

const CompletedRange = styled.div `
    height: 7px;
    background-color: rgba(195, 195, 195, 0.2);
    margin: 5px 16px 15px 0;
    width: 100%;
`;

const CompletedBar = styled.div `
    background: #ffd940;
`;

class CourseBanner extends Component<any, any> {

    constructor(props:any){
        super(props);
        
        this.state = {
            display: '',
            isOpenModal: false,
            btnState: 'Start'
        }; 
    }

    handleShowHide = (e:any) => {
        this.setState({
            display : 'block',
            btnState: 'Resume'
        });
    }

    toggleModal = () => {
        this.setState({
            isOpenModal: !this.state.isOpenModal
        });
    }

    render() {
        const videoJsOptions: any = {
            autoplay: false,
            controls: true,
            sources: [{
              src: 'https://www.w3schools.com/html/mov_bbb.mp4',
              type: 'video/mp4'
            }]
        };
        return (
            <CourseBannerWrapper style={{ paddingTop: 60, paddingBottom: 60 }}>
                <Grid>
                    <Row className="d-flex align-items-start">
                        <div className="col px-3">
                            <CourseBannerDetail>
                                <div className="font28 white-text font-bold mb-3">Fundamentals of Data Analytics</div>
                                <Link to="/dashboard">
                                    <ReturnBack className="icon icon-return-btn"></ReturnBack>
                                </Link>
                                <div className="font16 mb-5">
                                    Learn fundamental statistical concepts, that are  widely applicable in data analytics through
                                    course  lessons and solving business cases.
                                </div>
                                {this.state.display ? 
                                    <div className="d-flex justify-content-between align-items-center">
                                        <Link to="/course" className="btn btn-lg btn-outline-white">Resume</Link>
                                        <div className="d-flex justify-content-between ml-4 font14" id="toggleProgress">
                                            <div className="mr-4">Progress</div>
                                            <div className="mr-4">
                                                <CompletedRange className="border-radius-4 m-0 mt-2" style={{ width: 250 }}>
                                                    <CompletedBar className="border-radius-4 height100" style={{ width: '5%' }}>
                                                    </CompletedBar>
                                                </CompletedRange>
                                            </div>
                                            <div className="mr-4"><span className="courseCompletedPercentage"> 5% </span> Completed</div>
                                        </div>
                                    </div>
                                    : <div className="d-flex justify-content-between align-items-center">
                                        <a href="javascript:void(0)" className="btn btn-lg btn-outline-white btn-outline" onClick={this.handleShowHide}>{this.state.btnState}</a>
                                    </div>
                                }
                            </CourseBannerDetail>
                        </div>
                        <div className="col px-3">
                            <CourseBannerDetailVideo className="d-flex justify-content-end ml-auto" onClick={this.toggleModal}>
                                <img src={require('../../assets/images/thumbnail.jpg')}/>
                            </CourseBannerDetailVideo>
                        </div>
                    </Row>
                </Grid>
                <ModalComponent
                    className="modal-lg modalBodyOnly"
                    isOpenModal={this.state.isOpenModal}
                    toggleModal={this.toggleModal}
                    ModalTitle=""
                    >
                        <div>   
                            <VideoPlayer {...videoJsOptions} />
                        </div>
                    </ModalComponent>
            </CourseBannerWrapper>
        );
    }



   

}

export default CourseBanner;