import React, { Component } from "react";
import {Link} from 'react-router-dom';
import {Clearfix, Col, Grid, Panel, Row} from 'react-bootstrap';
import styled from 'styled-components';
import '../../assets/sass/module/CourseDetail.scss';
import Slider from "react-slick";
// import Arrow from "../../components/SliderSlickArrow/SlickArrow";

const CourseListWidget = styled.div `
    overflow: hidden;
`;

const CourseCardList = styled.div `
    border-radius: 10px;
    margin: 40px 90px;
`;

const CourseCardHeader = styled.div `
    padding: 32px 40px;
    background-color: rgba(255, 217, 64, 0.21);
`;

const CourseCardBody = styled.div `    
    padding: 32px 40px;
`;

export default class CourseList extends Component<any, any> {
    slider: any;
    constructor(props: any) {
        super(props);
    }
    
    handleArrow = (arrow: any) => {
        console.log(arrow)
    };

    render() {

        const pagination = [
            {
                id: 1,
                WeeksOngoing: "Week 1-2",
                active: true,
            },
            {
                id: 2,
                WeeksOngoing: "Week 3-4",
                active: false,
            },
            {
                id: 3,
                WeeksOngoing: "Week 5-8",
                active: false,
            },
            {
                id: 4,
                WeeksOngoing: "Week 9-10",
                active: false,
            },
            {
                id: 5,
                WeeksOngoing: "Week 11-12",
                active: false,
            }
        ]
        
        const settings = {
            dots: true,
            infinite: false,
            speed: 500,
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            customPaging: function (i: any) {
                return (
                    <div className={"courseWeekContainer d-flex flex-column align-items-center " + (pagination[i].active ? "true" : "false")} >
                        <div className="courseWeekButton"><button></button></div>
                        <a className="courseWeek mt-3">{pagination[i].WeeksOngoing}</a>
                        {pagination[i].active ? <i className="icon icon-arrow-down"></i> : ""}
                    </div>
                );
            },
        };

        return (
            <CourseListWidget>
                <Grid>
                    <Row>
                        <div className="col">
                            <Slider ref={slider => (this.slider = slider)} {...settings}>                            
                                <div>
                                    <CourseCardList className="courseCardList">
                                        <CourseCardHeader className="d-flex justify-content-center align-items-center">
                                            <div className="flex-column text-center width150px">
                                                <div className="courseTypeCount font-bold font24 d-block">03</div>
                                                <div className="courseTypeDescription font18 tertiary-text mt-2">Simulation</div>
                                            </div>
                                            <div className="flex-column text-center width150px">
                                                <div className="courseTypeCount font-bold font24 d-block">01</div>
                                                <div className="courseTypeDescription font18 tertiary-text mt-2">Quiz</div>
                                            </div>
                                            <div className="flex-column text-center width150px">
                                                <div className="courseTypeCount font-bold font24 d-block">01</div>
                                                <div className="courseTypeDescription font18 tertiary-text mt-2">Webinar</div>
                                            </div>
                                            <div className="flex-column text-center width150px">
                                                <div className="courseTypeCount font-bold font24 d-block">02</div>
                                                <div className="courseTypeDescription font18 tertiary-text mt-2">Solve</div>
                                            </div>
                                        </CourseCardHeader>
                                        <CourseCardBody>
                                            <div className="d-flex justify-content-between">
                                                <div>
                                                    <div className="secondary-text font-bold font18 mb-3">What you will learn</div>
                                                    <ul className="ml-3 tertiary-text font14">
                                                        <li className="mb-2">Learn how to compute basic descriptive statistics for numeric variables.</li>
                                                        <li className="mb-2">Learn how to compute basic bivariate descriptive statistic: Correlation</li>
                                                        <li className="mb-2">Provide an insight on how credit line & balance/ credit line & purchase are inter related</li>
                                                        <li className="mb-2">Complete all the simulations/solves/quizzes to get the badges and points</li>
                                                    </ul>
                                                </div>
                                                <div className="d-flex align-items-end mt-2">
                                                    <Link to="/course" className="btn btn-outline btn-lg btn-create">Resume</Link>
                                                </div>
                                            </div>
                                        </CourseCardBody>
                                    </CourseCardList>
                                </div>
                                <div>
                                    <CourseCardList className="courseCardList">
                                        <CourseCardHeader className="d-flex justify-content-center align-items-center">
                                            <div className="flex-column text-center width150px">
                                                <div className="courseTypeCount font-bold font24 d-block">06</div>
                                                <div className="courseTypeDescription font18 tertiary-text mt-2">Simulation</div>
                                            </div>
                                            <div className="flex-column text-center width150px">
                                                <div className="courseTypeCount font-bold font24 d-block">07</div>
                                                <div className="courseTypeDescription font18 tertiary-text mt-2">Quiz</div>
                                            </div>
                                            <div className="flex-column text-center width150px">
                                                <div className="courseTypeCount font-bold font24 d-block">08</div>
                                                <div className="courseTypeDescription font18 tertiary-text mt-2">Webinar</div>
                                            </div>
                                            <div className="flex-column text-center width150px">
                                                <div className="courseTypeCount font-bold font24 d-block">09</div>
                                                <div className="courseTypeDescription font18 tertiary-text mt-2">Solve</div>
                                            </div>
                                        </CourseCardHeader>
                                        <CourseCardBody>
                                            <div className="d-flex justify-content-between">
                                                <div>
                                                    <div className="secondary-text font-bold font18 mb-3">What you will learn</div>
                                                    <ul className="ml-3 tertiary-text font14">
                                                        <li className="mb-2">Learn how to compute basic descriptive statistics for numeric variables.</li>
                                                        <li className="mb-2">Learn how to compute basic bivariate descriptive statistic: Correlation</li>
                                                        <li className="mb-2">Provide an insight on how credit line & balance/ credit line & purchase are inter related</li>
                                                        <li className="mb-2">Complete all the simulations/solves/quizzes to get the badges and points</li>
                                                    </ul>
                                                </div>
                                                <div className="d-flex align-items-end">
                                                    <Link to="/course" className="btn btn-outline btn-lg btn-create">Start</Link>
                                                </div>
                                            </div>
                                        </CourseCardBody>
                                    </CourseCardList>
                                </div>
                                <div>
                                    <CourseCardList className="courseCardList">
                                        <CourseCardHeader className="d-flex justify-content-center align-items-center">
                                            <div className="flex-column text-center width150px">
                                                <div className="courseTypeCount font-bold font24 d-block">03</div>
                                                <div className="courseTypeDescription font18 tertiary-text mt-2">Simulation</div>
                                            </div>
                                            <div className="flex-column text-center width150px">
                                                <div className="courseTypeCount font-bold font24 d-block">01</div>
                                                <div className="courseTypeDescription font18 tertiary-text mt-2">Quiz</div>
                                            </div>
                                            <div className="flex-column text-center width150px">
                                                <div className="courseTypeCount font-bold font24 d-block">01</div>
                                                <div className="courseTypeDescription font18 tertiary-text mt-2">Webinar</div>
                                            </div>
                                            <div className="flex-column text-center width150px">
                                                <div className="courseTypeCount font-bold font24 d-block">02</div>
                                                <div className="courseTypeDescription font18 tertiary-text mt-2">Solve</div>
                                            </div>
                                        </CourseCardHeader>
                                        <CourseCardBody>
                                            <div className="d-flex justify-content-between">
                                                <div>
                                                    <div className="secondary-text font-bold font18 mb-3">What you will learn</div>
                                                    <ul className="ml-3 tertiary-text font14">
                                                        <li className="mb-2">Learn how to compute basic descriptive statistics for numeric variables.</li>
                                                        <li className="mb-2">Learn how to compute basic bivariate descriptive statistic: Correlation</li>
                                                        <li className="mb-2">Provide an insight on how credit line & balance/ credit line & purchase are inter related</li>
                                                        <li className="mb-2">Complete all the simulations/solves/quizzes to get the badges and points</li>
                                                    </ul>
                                                </div>
                                                <div className="d-flex align-items-end">
                                                    <Link to="/course" className="btn btn-outline btn-lg btn-create">Start</Link>
                                                </div>
                                            </div>
                                        </CourseCardBody>
                                    </CourseCardList>
                                </div>
                                <div>
                                    <CourseCardList className="courseCardList">
                                        <CourseCardHeader className="d-flex justify-content-center align-items-center">
                                            <div className="flex-column text-center width150px">
                                                <div className="courseTypeCount font-bold font24 d-block">03</div>
                                                <div className="courseTypeDescription font18 tertiary-text mt-2">Simulation</div>
                                            </div>
                                            <div className="flex-column text-center width150px">
                                                <div className="courseTypeCount font-bold font24 d-block">01</div>
                                                <div className="courseTypeDescription font18 tertiary-text mt-2">Quiz</div>
                                            </div>
                                            <div className="flex-column text-center width150px">
                                                <div className="courseTypeCount font-bold font24 d-block">01</div>
                                                <div className="courseTypeDescription font18 tertiary-text mt-2">Webinar</div>
                                            </div>
                                            <div className="flex-column text-center width150px">
                                                <div className="courseTypeCount font-bold font24 d-block">02</div>
                                                <div className="courseTypeDescription font18 tertiary-text mt-2">Solve</div>
                                            </div>
                                        </CourseCardHeader>
                                        <CourseCardBody>
                                            <div className="d-flex justify-content-between">
                                                <div>
                                                    <div className="secondary-text font-bold font18 mb-3">What you will learn</div>
                                                    <ul className="ml-3 tertiary-text font14">
                                                        <li className="mb-2">Learn how to compute basic descriptive statistics for numeric variables.</li>
                                                        <li className="mb-2">Learn how to compute basic bivariate descriptive statistic: Correlation</li>
                                                        <li className="mb-2">Provide an insight on how credit line & balance/ credit line & purchase are inter related</li>
                                                        <li className="mb-2">Complete all the simulations/solves/quizzes to get the badges and points</li>
                                                    </ul>
                                                </div>
                                                <div className="d-flex align-items-end">
                                                    <Link to="/course" className="btn btn-outline btn-lg btn-create">Start</Link>
                                                </div>
                                            </div>
                                        </CourseCardBody>
                                    </CourseCardList>
                                </div>
                                <div>
                                    <CourseCardList className="courseCardList">
                                        <CourseCardHeader className="d-flex justify-content-center align-items-center">
                                            <div className="flex-column text-center width150px">
                                                <div className="courseTypeCount font-bold font24 d-block">03</div>
                                                <div className="courseTypeDescription font18 tertiary-text mt-2">Simulation</div>
                                            </div>
                                            <div className="flex-column text-center width150px">
                                                <div className="courseTypeCount font-bold font24 d-block">01</div>
                                                <div className="courseTypeDescription font18 tertiary-text mt-2">Quiz</div>
                                            </div>
                                            <div className="flex-column text-center width150px">
                                                <div className="courseTypeCount font-bold font24 d-block">01</div>
                                                <div className="courseTypeDescription font18 tertiary-text mt-2">Webinar</div>
                                            </div>
                                            <div className="flex-column text-center width150px">
                                                <div className="courseTypeCount font-bold font24 d-block">02</div>
                                                <div className="courseTypeDescription font18 tertiary-text mt-2">Solve</div>
                                            </div>
                                        </CourseCardHeader>
                                        <CourseCardBody>
                                            <div className="d-flex justify-content-between">
                                                <div>
                                                    <div className="secondary-text font-bold font18 mb-3">What you will learn</div>
                                                    <ul className="ml-3 tertiary-text font14">
                                                        <li className="mb-2">Learn how to compute basic descriptive statistics for numeric variables.</li>
                                                        <li className="mb-2">Learn how to compute basic bivariate descriptive statistic: Correlation</li>
                                                        <li className="mb-2">Provide an insight on how credit line & balance/ credit line & purchase are inter related</li>
                                                        <li className="mb-2">Complete all the simulations/solves/quizzes to get the badges and points</li>
                                                    </ul>
                                                </div>
                                                <div className="d-flex align-items-end">
                                                    <Link to="/course" className="btn btn-outline btn-lg btn-create">Start</Link>
                                                </div>
                                            </div>
                                        </CourseCardBody>
                                    </CourseCardList>
                                </div>
                            </Slider>
                        </div>
                    </Row>
                </Grid>
            </CourseListWidget>
        )
    }
}