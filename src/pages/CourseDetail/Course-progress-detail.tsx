import React,{ Component } from 'react';
import styled, { injectGlobal } from 'styled-components';
import {Clearfix, Col, Grid, Panel, Row} from 'react-bootstrap';
import '../../assets/sass/module/Dashboard.scss';

const CourseProgressDetailCard = styled.div `
    padding: 30px;
    box-shadow: 0 0 6px 0 rgba(0, 0, 0, 0.16);
    width: 350px;
    min-height: 400px;
    border-radius: 10px;
`;

const CourseProgressElements = styled.div `
    background-color: rgba(255, 217, 64, 0.21);
    border-radius: 50%;
    height: 70px;
`;

const CourseProgressDetail = (props:any) => {
    return (
        <CourseProgressDetailCard>
            <div className="d-flex justify-content-center align-items-center">
                <Row>
                    <Col md={6} className="d-flex flex-column align-items-center mb-3">
                        <CourseProgressElements className="d-flex justify-content-center align-items-center width70px p-3 mb-2">
                            <i className="icon icon-courseProgress-calander"></i>
                        </CourseProgressElements>
                        <div className="courseProgressElementsDetail font18">12  Weeks</div>
                    </Col>
                    <Col md={6} className="d-flex flex-column align-items-center mb-4">
                        <CourseProgressElements className="d-flex justify-content-center align-items-center width70px p-3 mb-2">
                            <i className="icon icon-courseProgress-simulation"></i>
                        </CourseProgressElements>
                        <div className="courseProgressElementsDetail font18">19 Simulations</div>
                    </Col>
                    <Col md={6} className="d-flex flex-column align-items-center mb-4">
                        <CourseProgressElements className="d-flex justify-content-center align-items-center width70px p-3 mb-2">
                            <i className="icon icon-courseProgress-quiz"></i>
                        </CourseProgressElements>
                        <div className="courseProgressElementsDetail font18">12 Quizzes</div>
                    </Col>
                    <Col md={6} className="d-flex flex-column align-items-center mb-4">
                        <CourseProgressElements className="d-flex justify-content-center align-items-center width70px p-3 mb-2">
                            <i className="icon icon-courseProgress-certificate"></i>
                        </CourseProgressElements>
                        <div className="courseProgressElementsDetail font18">01  Certificate</div>
                    </Col>
                    <Col md={12} className="d-flex flex-column align-items-center font14 mt-2">
                        <div className="justify-content-between pb-3">
                            <div> Start : <span className="font-bold ml-2"> 27 Nov 2018</span>  </div>
                        </div>
                        <div className="justify-content-between">
                            <div> Days Remaining : <span className="font-bold ml-2"> 83 Days</span>  </div>
                        </div>
                    </Col>
                </Row>
            </div>
        </CourseProgressDetailCard>
    );
}

export default CourseProgressDetail;