import React, { Component } from 'react';
import '../../assets/sass/module/CourseDetail.scss';
import styled, { injectGlobal } from 'styled-components';
import {Clearfix, Col, Grid, Panel, Row} from 'react-bootstrap';
import HeaderMenu from '../../components/HeaderMenu';
import CourseBanner from './Course-detail-banner';
import CourseProgressDetail from './Course-progress-detail';
import CourseList from './Course-list';
import Scrollbars from 'react-custom-scrollbars';

const ScrollbarWithMenu = styled.section `
    height: calc(100vh - 60px);
`;

const CourseDescriptionWrapper = styled.div `
    padding: 34px 0;
`;

const CourseDescriptionDetail = styled.div `
    padding: 16px 100px 0 0;
    @media screen and (max-width: 1366px){
        padding: 16px 16px 0 0;
    }
`;

export default class CourseDetails extends Component {
    render() {
        return (
            // course Details Section
            //Course Detail Description
            <ScrollbarWithMenu>
                <HeaderMenu/>
                <Scrollbars className="custom-scrollbars">
                    <CourseBanner />
                    <CourseDescriptionWrapper>
                        <Grid>
                            <Row>
                                <Col md={8}>
                                    <CourseDescriptionDetail>
                                        <div className="courseDescription text-uppercase secondary-text font16 font-bold pb-3">Description :</div>
                                        <div className="CourseSubDescription font14 tertiary-text pb-34 line-height-24">
                                            Data Analytics is rapidly becoming one of the most critical drivers for any decision-making, at an 
                                            individual or a business level. At the heart of Data analytics, lies the fundamentals of statistics. This 
                                            course will help you learn basic statistical concepts with practical problem solving and interpretation 
                                            through application of the theoretical learnings. The course has been divided into 3 modules with 
                                            Simulations, Quizzes and Solve within each of those.
                                        </div>
                                        <div className="courseDescriptionLearning text-uppercase secondary-text font16 font-bold pb-3">Learning Objective :</div>
                                        <div className="CourseSubDescriptionLearning font14 tertiary-text line-height-24">
                                            Learn fundamental statistical concepts, that are widely applicable in data analytics through course 
                                            lessons and solving business cases. Apply the knowledge gained to solve business problems through 
                                            simulations using real data, validate your knowledge by answering quiz questions under each 
                                            module and finally test your understanding by solving real problems under the Solve section. At the 
                                            end of this course, you should be able to understand data type and their representation, apply 
                                            descriptive statistical measures to interpret data and make statistical inferences based on the data 
                                            distribution and use of appropriate statistical tests. 
                                        </div>
                                    </CourseDescriptionDetail>
                                </Col>
                                <Col md={4}>
                                    <div className="courseProgress-detail">
                                        <CourseProgressDetail />
                                    </div>
                                </Col>
                            </Row>
                        </Grid>
                    </CourseDescriptionWrapper>

                    <div className="courseList-wrapper">
                        <CourseList />
                    </div>

                </Scrollbars>
            </ScrollbarWithMenu>
        );
    }
}


