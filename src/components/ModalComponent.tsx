import React, { Component } from 'react';
import { Button } from './FormElements';
import '../assets/sass/module/Quiz.scss';
import '../assets/sass/module/Modal.scss';
import { Modal } from 'react-bootstrap';
import styled from 'styled-components';

const ModalTitle = styled.p `
    font-size: 24px;
    margin: 0;
    color: #302a25;
`;


export default class ModalComponent extends Component<any, any> {
    constructor(props:any){
        super(props);
    }

    render(){
        return(
            <Modal show={this.props.isOpenModal} onHide={this.props.toggleModal} className={this.props.className}>
                <Modal.Header closeButton>
                    <ModalTitle>{this.props.ModalTitle}</ModalTitle>
                </Modal.Header>
                <Modal.Body>
                    {this.props.children}
                </Modal.Body>
                <Modal.Footer>
                    <Button color="secondary" onClick={this.props.toggleModal}>Cancel</Button>
                </Modal.Footer>
            </Modal>
        );
    }
}