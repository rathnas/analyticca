import React, { Component } from "react";
import _ from "lodash";
import CircularProgressbar from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";

class ChangingProgressbar extends React.Component<any, any> {
    constructor(props:any) {
        super(props);
        this.state = {
            currentPercentageIndex: 0,
            textPercentage: this.props.textForPercentage,
            hms: this.props.timer,
            closeTime: 1,
        }
    }

    componentDidMount = () => {
        setInterval(() => {
            let d = Number(this.state.textPercentage-1);
            // var h = Math.floor(d / 3600);  /* ----------- To display Hours */
            var m = Math.floor(d % 3600 / 60);
            var s = Math.floor(d % 3600 % 60);
            // var hDisplay = (h > 0) ? h : "";  /* ----------- To display Hours */
            var mDisplay = (m > 0) ? m : "";
            var sDisplay = (s > 0) ? s : "";

            m == 0 && s == 0 ?
                this.setState({
                    currentPercentageIndex: Math.round((this.state.closeTime) % this.props.percentages.length),
                    // hms: ('00' + hDisplay).slice(-2) + ':' + ('00' + mDisplay).slice(-2) + ':' + ('00' + sDisplay).slice(-2),  /* ----------- To display Hours */                                
                    hms: ('00' + mDisplay).slice(-2) + ':' + ('00' + sDisplay).slice(-2),
                })
            :
                this.setState({
                    currentPercentageIndex: Math.round((this.state.currentPercentageIndex + 1) % this.props.percentages.length),
                    textPercentage: this.state.textPercentage - 1,
                    // hms: ('00' + hDisplay).slice(-2) + ':' + ('00' + mDisplay).slice(-2) + ':' + ('00' + sDisplay).slice(-2),  /* ----------- To display Hours */
                    hms: ('00' + mDisplay).slice(-2) + ':' + ('00' + sDisplay).slice(-2),
                    closeTime: this.state.closeTime + 1
                });

        }, this.props.interval);
    }

    getStyles = () => {
        return this.props.stylesForPercentage ? this.props.stylesForPercentage(this.getCurrentPercentage()) : {};
    }

    getCurrentPercentage = () => {
        return this.props.percentages[this.state.currentPercentageIndex];
    }

    render() {
        return (
        <CircularProgressbar
            {...this.props}
            percentage={this.getCurrentPercentage()}
            strokeWidth={5}
            text={this.state.hms}
            styles={{
                root: {},
                path: {
                  stroke: '#019c3a',
                  transition: 'stroke-dashoffset 0.5s ease 0s',
                },
                trail: {
                  stroke: '#e1e1e1',
                },
                text: {
                  fill: '#019c3a',
                  fontSize: '16px',
                },
              }}
        />
        );
    }
}

const CountdownProgressbar = (props:any) => {
    const secondsToPercentages = _.range(0, props.numSeconds + 1).map( seconds => seconds / props.numSeconds * 100 );

    return (
        <ChangingProgressbar
            percentages={secondsToPercentages}
            textForPercentage={props.numSeconds}
            interval={1000}
        />
    );
}

const timerFunction = (timer:any) => {
    let parts = timer.split(':');
    // let hours = +parts[0]  /* ----------- To display Hours */
    let minutes = +parts[0]
    let seconds = +parts[1]
    return (
        <div className="ProgressTimerContainer">
            {/* <CountdownProgressbar numSeconds={hours * 60 * 60 + minutes * 60 + seconds} time={timer} />   ----------- To display Hours */}
            <CountdownProgressbar numSeconds={minutes * 60 + seconds} time={timer} />
        </div>
    )
}

export const TimerProgressbar = (props:any) => (
    timerFunction(props.timer)
);