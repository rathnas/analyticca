import React, { Component } from 'react';
import BrandImg from '../components/BrandImg';
import '../assets/sass/module/HeaderMenu.scss';
import { NavLink } from 'react-router-dom';
import { Grid, Navbar, Nav, NavItem, NavDropdown, Dropdown, MenuItem } from 'react-bootstrap';
import { Button, Input } from './FormElements';
import styled, { injectGlobal } from 'styled-components';

    const ProfileContent = styled.div`

    `;

    const NavBar = styled(Navbar) `
        box-shadow: 0 3px 7px rgba(0, 0, 0, 0.07) !important;
    `;

    const BtnCreate = styled(Button)`
        position: relative;
        margin-right: 10px;
        i{
            position: relative;
            top: -2px;
        }
        &:hover{
            i{
                background-position: -2px -18px;
            }
        }
    `;

    const ProfileImage = styled.div`
        height: 30px;
        width: 30px;
        overflow: hidden;
        border-radius: 100%;
        img{
            height: 100%;
        }
    `;

    const ProfileName = styled.div`
        font-size: 12px;
        font-weight: 600;
        color: #3c3c3c;
    `;

    const ProfileCaret = styled.div `
        i{
            transition: all linear .16s;
            transform: rotate(0deg);
        }
    `;

    const NavbarRight = styled(Nav) `
        .open{
            .dropdown-toggle{
                box-shadow: none;
                background-color: #eaeaea;
                &#notification{
                    background-color: #ffffff;
                }
                i.caret-icon{
                    transform: rotate(180deg);
                }
            }
        }
        .dropdown-toggle{
            border-radius: 0;
            padding: 0;
            transition: all linear .16s;
            &#notification{
                margin: 0 10px;
            }
            &#ProfileDropdown{
                padding: 5px 16px;
                &:hover{
                    background-color: #eaeaea;
                }
            }
        }
    `;

    const NotificationLink = styled.span`
        position: relative;
    `;

    const NotifyNumber = styled.span `
        position: absolute;
        font-weight: 600;
        height: 16px;
        width: 16px;
        top: -7px;
        right: -2px;
        text-align: center;
        color: #ffffff;
        font-size: 9px;
        padding: 2px;
        background-color: #ad0000;
        border-radius: 100%;
    `;

    const NavMenu = styled(Nav) `
        height: 60px;
    `;

    const NavBrand = styled(Navbar.Brand) `
        height: 60px;
        margin: 0 !important;
        padding: 20px;
    `;

    const NavbarLink = styled(NavLink) `
        display: inline-block;
        color: #3c3c3c;
        padding: 20px 16px;
        font-weight: normal;
        background-color: #ffffff;
        transition: all linear 0.16s;
        &:hover{
            color: #ad0000;
            font-weight: 600;
            background-color: #eaeaea;
        }
        &.active{
            color: #ad0000;
            font-weight: 600;
        }
    `;

export default class HeaderMenu extends React.Component<any, any> {

    constructor(props:any) {
        super(props);
        this.state = {
            isOpen: false,
            notification: false,
            profileDropdown: false,
            menu: [
                {
                    name: 'Home',
                    status: false,
                    link: "/",
                    id: "home"
                },
                {
                    name: 'Projects',
                    status: false,
                    link: "/dashboard",
                    id: "projects"
                },
                {
                    name: 'Portfolio',
                    status: false,
                    link: "/",
                    id: "portfolio"
                },
                {
                    name: 'Help',
                    status: false,
                    link: "/",
                    id: "help"
                },
                {
                    name: 'Community',
                    status: true,
                    link: "/",
                    id: "community"
                },
            ]
        };
    }

    handleOpen = () => {
        this.setState({ isOpen: true })
    }

    handleClose = () => {
        this.setState({ isOpen: false })
    }

    componentDidMount = () => {
        document.body.style.padding = '60px 0 0 0';
    }

    render() {
        let menuElements = this.state.menu.map((elem:any) => {
            return (
                <NavbarLink className={elem.status ? 'active' : ''} key={elem.name} exact to={elem.link}>{elem.name}</NavbarLink >
            )
        });

        return (
            <Grid fluid>
                <NavBar className="navbar-fixed-top" fluid>
                    <NavBrand href="/"><BrandImg/></NavBrand>
                    <NavMenu className="mr-auto">
                        {menuElements}
                    </NavMenu>
                    <NavbarRight className="navbar-right">
                        <BtnCreate className="btn btn-outline btn-sm" type="submit"><i className="icon icon-plus-sm mr-2"></i> CREATE COURSE</BtnCreate>
                        <Dropdown id="notification">
                            <Dropdown.Toggle noCaret>
                                <NotificationLink>
                                    <i className="icon icon-notification-bell"></i>
                                    <NotifyNumber>21</NotifyNumber>
                                </NotificationLink>
                            </Dropdown.Toggle>
                            <Dropdown.Menu>
                                <MenuItem className="" href="/">Header</MenuItem>
                                <MenuItem className="" href="/">Header</MenuItem>
                            </Dropdown.Menu>
                        </Dropdown>
                        <Dropdown onMouseEnter = {this.handleOpen} onMouseLeave = { this.handleClose } id="ProfileDropdown">
                            <Dropdown.Toggle noCaret>
                                <ProfileContent className="d-flex align-items-center">
                                    <ProfileImage><img src={require('../assets/images/users/male1.jpg')} /></ProfileImage>
                                    <ProfileName className="text-capitalize mx-2">NELSON JOHN</ProfileName>
                                    <ProfileCaret className="mb-1"><i className="icon icon-down-color caret-icon"></i></ProfileCaret>
                                </ProfileContent>
                            </Dropdown.Toggle>
                            <Dropdown.Menu>
                                <MenuItem className="" href="/">My Profile</MenuItem>
                                <MenuItem className="" href="/">Logout</MenuItem>
                            </Dropdown.Menu>
                        </Dropdown>
                    </NavbarRight>
                </NavBar>
            </Grid>
        );
    }
}