import React from 'react';

class Button extends React.Component <any> {
    constructor(props:any){
        super(props)
    };
  render() {
    return (
        <button
            className={"btn " + this.props.className}
            onClick={this.props.onClick}>
            {this.props.children}
        </button>
    );
  }
}

class Input extends React.Component <any> {
    constructor(props:any){
        super(props)
    };
  render() {
    return (
        <input
            type={this.props.inputType}
            className={this.props.className}
            placeholder={this.props.placeHolder}
        />
    );
  }
}

export{
    Button as Button,
    Input as Input
}