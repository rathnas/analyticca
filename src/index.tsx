import React from 'react';
import ReactDOM from 'react-dom';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './assets/sass/index.scss';
import Routes from './routes';

ReactDOM.render(<Routes />, document.getElementById('root'));